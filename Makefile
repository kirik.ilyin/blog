run:
	docker-compose up -d

prepare-laravel:
	docker-compose down
	docker-compose up -d
	sudo chown ${USER}:${USER} -R master
	sudo chmod -R 777 master/bootstrap/cache
	sudo chmod -R 777 master/storage
	docker-compose exec pressportal_master_1 php artisan key:generate --ansi
	#docker-compose exec php composer require predis/predis
	docker-compose exec pressportal_master_1 php artisan --version
