<?php

namespace App\Providers;


use App\Master\Contracts;
use App\Master\Repositories\ElasticSearchRepository;
use App\Master\Repositories\PostRepository;
use App\Models\Posts;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class MasterServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories();
        $this->registerServices();
    }

    private function registerRepositories()
    {
//        $this->app->bind(Contracts\Repositories\PostRepository::class, function () {
//            return new PostRepository(new Posts());
//        });

        $this->app->bind(Contracts\Repositories\PostRepository::class, function () {
            // Это полезно, если мы хотим выключить наш кластер
            // или при развертывании поиска на продакшене
            if (! config('services.search.enabled')) {
                return new PostRepository(new Posts());
            }
            return new ElasticSearchRepository(
                new Posts(),
                $this->app->make(Client::class)
            );
        });

        $this->bindSearchClient();
    }

    private function bindSearchClient()
    {
        $this->app->bind(Client::class, function ($app) {
            return ClientBuilder::create()
                ->setHosts($app['config']->get('services.search.hosts'))
                ->build();
        });
    }

    private function registerServices()
    {

    }

    public function provides()
    {
        return [
            Contracts\Repositories\PostRepository::class,
        ];
    }
}
