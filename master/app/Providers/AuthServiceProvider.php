<?php

namespace App\Providers;

use App\Models\Role;
use App\Models\Team;
use App\Models\User;
use App\Policies\AdminPolicy;
use App\Policies\RolesPolicy;
use App\Policies\TeamPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Team::class => TeamPolicy::class,
        Role::class => AdminPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

//        Gate::before(function (?User $user){
//            if($user){
//                $roles = $user->roles->pluck('code');
//                return $roles->contains('admin');
//            }
//        }); // Если true все остальные проверки пропускаются

        Gate::define('admin-views', function (?User $user){
            if($user){
                $roles = $user->roles->pluck('code');
                return $roles->contains('admin');
            }
            return false;
        });
        //
    }
}
