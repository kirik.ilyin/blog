<?php

namespace App\Console\Commands;

use App\Models\Posts;
use Elasticsearch\Client;
use Illuminate\Console\Command;


class ReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:reindex';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexes all articles to Elasticsearch';
    /** @var \Elasticsearch\Client */
    private $elasticsearch;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $elasticsearch)
    {
        parent::__construct();
        $this->elasticsearch = $elasticsearch;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // сначала нужно создать индекс с анализаторами, после заполнять данными
        $this->info('Indexing all articles. This might take a while...');

        $index = app(Posts::class)->getSearchIndex();

        if ($this->elasticsearch->indices()->exists(['index' => $index])) {
            $this->output->write('Delete');
            $result = ($this->elasticsearch->indices()->delete(['index' => $index]));
        }

        $params = [
            'index' => $index,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 1,
                    'analysis' => [
                        'filter' => [
                            "russian_stop" => [
                                "type" => "stop",
                                "stopwords" => "_russian_"
                            ],
                            "russian_keywords" => [
                                "type" => "keyword_marker",
                                "keywords" => ["пример"]
                            ],
                            "russian_stemmer" => [
                                "type" => "stemmer",
                                "language" => "russian"
                            ]
                        ],
                        'analyzer' => [
                            'rebuilt_russian' => [
                                'type' => 'custom',
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase', 'russian_stop', 'russian_keywords', 'russian_stemmer']
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    'properties' => [
                        'active' => ['type' => 'boolean'],
                        'created_at' => ['type' => 'text'],
                        'id' => ['type' => 'text'],// Убрать
                        'text' => [
                            'type' => 'text',
                            'analyzer' => 'rebuilt_russian',
                        ],
                        'title' => [
                            'type' => 'text',
                            'analyzer' => 'rebuilt_russian',
                        ],
                        'updated_at' => ['type' => 'keyword'],
                        'url_code' => ['type' => 'keyword'],
                        'user_id' => ['type' => 'keyword'],
                        'tags' => ['type' => 'keyword'],
                    ]
                ]
            ]
        ];
        $this->output->write('Index create. Loading data');

        $this->elasticsearch->indices()->create($params);

        foreach (Posts::cursor() as $article) {
            $this->elasticsearch->index([
                'index' => $article->getSearchIndex(),
                'id' => $article->getKey(),
                'body' => $article->toSearchArray(),
            ]);
            $this->output->write('.');
        }
        $this->info('\nDone!');
    }
}
