<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class PostsController extends Controller
{
//    public function __construct(){
//        $this->middleware('auth:api', ['except' => ['index','show']]);
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $posts = Posts::latest()->get(); //only
        return Inertia::render('Admin/Post/index', [
            'posts' => $posts,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Admin/Post/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $attr = $this->validate(request(),[
            'url_code' => 'required',
            'title' => 'required',
            'text' => 'required',
            'preview_image' => 'required',
            'high_image' => 'required',
            'low_image' => 'required',
        ]);
        //$request->file('avatar')->getClientOriginalName();
//        $path = $request->file('avatar')->storeAs(
//            'avatars', $request->user()->id
//        );
        $pathPreviewImage = \Storage::put('preview_images',$attr['preview_image']);
        $pathHighImage = \Storage::put('high_image',$attr['high_image']);
        $pathLowImage = \Storage::put('low_image',$attr['low_image']);
//       todo - $id = Image->create($pathPreviewImage); Posts::create('preview_image_id'=>$id)....
        dd($attr);
        $attr['user_id'] = Auth::user()->id;
        Posts::create($attr);
        return redirect(route('posts.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Inertia\Response
     */
    public function edit(Posts $post)
    {
        return Inertia::render('Admin/Post/edit', [
            'post' => $post,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $post)
    {
        $attr = $this->validate(request(),[
            'url_code' => 'required',
            'title' => 'required',
            'text' => 'required',
        ]);
        $post->update(
            $attr,
            [
                'user_id' => Auth::user()->id,
            ]
        );
        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posts $post)
    {
        $post->delete();
        return redirect(route('posts.index'));
    }
}
