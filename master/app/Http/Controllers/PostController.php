<?php

namespace App\Http\Controllers;

use App\Master\Contracts\Repositories\PostRepository;
use App\Models\Posts;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PostController extends Controller
{
    private $repository;

    public function __construct()
    {
        $this->repository = app(PostRepository::class);
    }

    public function index()
    {
        $posts = $this->repository->paginate(8);
        return Inertia::render('Post/index', [
            'posts' => $posts,
        ]);
    }

    public function search(Request $request)
    {
        $posts = $this->repository->search($request);
        dd($posts);
    }
}
