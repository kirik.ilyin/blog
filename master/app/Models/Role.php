<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Role extends Model
{
    use HasUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['name', 'code'];

    protected $casts = [
        'updated_at' => 'date:M j, Y, g:i a',
        'created_at' => 'date:M j, Y, g:i a',
    ]; //Конвертация значений, особенно удобно если нужно хранить данные в json формате, то в sql нужно создать поле text и класть туда просто массив невозможно и будет ошибка, для этого просто добавить поле и array, напр - 'options' => 'array'

    public function users(){
        return $this->belongsToMany(User::class, 'user_role', 'role_id');
    }
}
