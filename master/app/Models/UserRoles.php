<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserRoles extends Model
{
    protected $fillable = [
        'dealer_id',
        'role_id',
        'employee_id',
    ];

    public $timestamps = false;

    protected $primaryKey = false;

    public $incrementing = false;

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id' );
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'employee_id');
    }

}
