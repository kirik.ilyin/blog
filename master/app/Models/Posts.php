<?php

namespace App\Models;

use App\Traits\HasUuid;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
    use HasFactory;
    use SoftDeletes; //Трейт для мягкого удаления (добавляет поле deleted_at и не отображает если удалено на сайте, но в бд сохраняется)
    use Searchable;
    use HasUuid;

    public $guarded = ['id']; // защита от массового заполнения полей
    public $fillable = []; // поля доступные для массового заполнения

    protected $casts = [
        'active' => 'boolean',
        'updated_at' => 'date:M j, Y, g:i a',
        'created_at' => 'date:M j, Y, g:i a',
    ]; //Конвертация значений, особенно удобно если нужно хранить данные в json формате, то в sql нужно создать поле text и класть туда просто массив невозможно и будет ошибка, для этого просто добавить поле и array, напр - 'options' => 'array'

    protected $hidden = [
        'deleted_at',
    ];

//    protected $dispatchesEvents = [
//        'created' => \App\Events\PostCreated::class,
//    ];

    public function scopeRelevant($query){
        return $query->where('active', true);
    }

    public function owner(){
        return $this->belongsTo(User::class,'user_id');
    }
}
