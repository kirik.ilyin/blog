<?php

namespace App\Master\Repositories;

use App\Master\Contracts\Repositories\PostRepository as PostContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PostRepository extends AbstractRepository implements PostContract
{
    public function findAllActive(): ?Model
    {
        return $this->getModel()->paginate(5);
    }

    public function paginate($limit = 5): ?LengthAwarePaginator
    {
        return $this->getModel()->paginate($limit);
    }

    public function search(string $query = ''): Collection
    {
        return $this->getModel()->query()
            ->where('text', 'like', "%{$query}%")
            ->orWhere('title', 'like', "%{$query}%")
            ->get();
    }
}
