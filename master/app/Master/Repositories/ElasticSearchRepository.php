<?php

namespace App\Master\Repositories;

use App\Master\Contracts\Repositories\PostRepository as PostContract;
use Elasticsearch\Client;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;

class ElasticSearchRepository extends AbstractRepository implements PostContract
{
    /** @var \Elasticsearch\Client */
    private $elasticsearch;

    public function __construct(Model $model, Client $elasticsearch)
    {
        parent::__construct($model);
        $this->elasticsearch = $elasticsearch;
    }

    public function findAllActive(): ?Model
    {
        return $this->getModel()->paginate(5);
    }

    public function paginate($limit = 5): ?LengthAwarePaginator
    {
        return $this->getModel()->paginate($limit);
    }

    public function search(Request $request): Collection
    {
        $items = $this->searchOnElasticsearch($request);
        return $this->buildCollection($items);
    }

    private function searchOnElasticsearch(Request $request): array
    {
        $items = $this->elasticsearch->search([
            'index' => $this->getModel()->getSearchIndex(),
            'body' => [
                'query' => [
                    'bool' => [
                        'filter' => [
                            'term' => [
                                'tags' => 'php'
                            ]
                        ]
                    ]
//                    'multi_match' => [
//                        'fields' => ['tags^5', 'title', 'text'],
//                        'query' => $request->tags,
//                    ],
                ],
            ],
            'size' => 300,
        ]);

        $itemsAggs = $this->elasticsearch->search([
            'index' => $this->getModel()->getSearchIndex(),
            'body' => [
                "aggregations" => [
                    "the_name" => [
                        "terms" => [
                            "field" => "tags"
                        ]
                    ]
                ]
            ],
            'size' => 300,
        ]);
        dd($items);
        return $items;
    }

    private function buildCollection(array $items): Collection
    {
        $ids = Arr::pluck($items['hits']['hits'], '_id');
        return $this->getModel()->findMany($ids)
            ->sortBy(function ($article) use ($ids) {
                return array_search($article->getKey(), $ids);
            });
    }
}
