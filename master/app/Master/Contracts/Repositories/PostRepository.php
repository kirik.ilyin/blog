<?php

namespace App\Master\Contracts\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface PostRepository
{
    public function findAllActive(): ?Model;
    public function paginate($limit = 5): ?LengthAwarePaginator;
    public function search(Request $request): Collection;
}
