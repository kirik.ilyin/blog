<?php

use Illuminate\Foundation\Application;
//use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::group(['prefix' => 'admin','middleware' => 'auth:sanctum', 'verified'], function() {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
    Route::resource('/posts', Controllers\Admin\PostsController::class);
    Route::resource('/roles', Controllers\Admin\RolesController::class);
    Route::resource('/users', Controllers\Admin\UsersController::class);
});

Route::group(['prefix' => 'posts'], function () {
    Route::get('/', [Controllers\PostController::class, 'index']);
    Route::get('/search', [Controllers\PostController::class, 'search']);
});

//Route::get('/posts')
