<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_bank', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('preview_photo_path', 2048)->nullable();
            $table->string('low_photo_path', 2048)->nullable();
            $table->string('high_photo_path', 2048)->nullable();
            $table->string('alt')->nullable();
            $table->string('title')->nullable();
            $table->morphs('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_bank');
    }
}
