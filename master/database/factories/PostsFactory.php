<?php
//php artisan db:seed --class=PostsTableSeeder
namespace Database\Factories;

use App\Models\Posts;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;

class PostsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Posts::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $tags = ['php', 'javascript', 'python', 'sport', 'golang'];

        return [
            'url_code' => $this->faker->word(),
            'title' => $this->faker->sentence(),
            'text' => $this->faker->text(),
            'tags' => $tags[array_rand($tags)],
            'user_id' => User::first()->id,
        ];
    }
}
